import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import * as React from 'react';
import HomeScreen from 'screens/home';
import TransferScreen from 'screens/transfer';

moment.locale('pt-br');

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="Home"
          component={HomeScreen}
        />
        <Stack.Screen name="Transfer" component={TransferScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
