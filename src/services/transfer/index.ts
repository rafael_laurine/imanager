import AsyncStorage from '@react-native-community/async-storage';
import { ITransfer } from 'models';
import { STATEMENT } from 'utils/constants';

const TransferService = {
  async fetchAll() {
    try {
      const jsonstring = await AsyncStorage.getItem('@user_statement');
      const data: ITransfer[] = JSON.parse(jsonstring || '[]');
      return Promise.resolve(
        data.sort((a, b) => {
          if (a.date < b.date) {
            return 1;
          }
          if (a.date > b.date) {
            return -1;
          }
          return 0;
        })
      );
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async sendTransfer(transfer: ITransfer) {
    try {
      const jsonstring = await AsyncStorage.getItem(STATEMENT);
      let data: ITransfer[] = JSON.parse(jsonstring || '[]');
      data.push(transfer);
      await AsyncStorage.setItem(STATEMENT, JSON.stringify(data));
      return Promise.resolve('Tranferência realizada com sucesso!');
    } catch (error) {
      return Promise.reject('Houve um erro na transferência. Tente novamente.');
    }
  }
};

export default TransferService;
