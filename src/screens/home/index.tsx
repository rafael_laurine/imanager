import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { ITransfer } from 'models';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  ListRenderItemInfo,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import TransferService from 'services/transfer';
import { converMoney } from 'utils';
import styles from './styles';

function HomeScreen() {
  const navigation = useNavigation();

  const [data, setData] = useState<ITransfer[]>([]);
  const [totalAmount, setTotalAmount] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);

  useFocusEffect(
    React.useCallback(() => {
      fetchStatement();
    }, [])
  );

  useEffect(() => {
    getTotalAmount();
  }, [data]);

  async function fetchStatement() {
    try {
      setLoading(true);
      const data = await TransferService.fetchAll();
      setData(data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  function getTotalAmount() {
    let totalAmount: number = 0;
    data.forEach((e) => {
      console.log(converMoney(e.amount));
      totalAmount += converMoney(e.amount) * (e.type == 'credit' ? -1 : 1);
    });
    setTotalAmount(totalAmount);
  }

  function onTransfer(type: 'credit' | 'debit') {
    navigation.navigate('Transfer', { type });
  }

  function renderItem(info: ListRenderItemInfo<ITransfer>) {
    const { description, amount, date, type } = info.item;
    return (
      <View style={styles.cell}>
        <View
          style={[
            styles.cellStatus,
            { backgroundColor: type == 'credit' ? 'tomato' : '#2196F3' }
          ]}
        />
        <View style={styles.cellInfo}>
          <Text style={styles.cellDate} children={moment(date).calendar()} />
          <Text style={styles.cellDescription} children={description} />
        </View>
        <Text style={styles.cellAmount} children={amount} />
      </View>
    );
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.card}>
        <Text children="Saldo atual" style={styles.cardSubtitle} />
        <Text
          children={`R$ ${totalAmount.toFixed(2)}`}
          style={styles.cardTitle}
        />
        <View style={styles.actionContainer}>
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => onTransfer('debit')}>
            <Text style={styles.actionLabel} children="Depositar" />
          </TouchableOpacity>
          <View style={{ width: 16 }} />
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => onTransfer('credit')}>
            <Text style={styles.actionLabel} children="Transferir" />
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
        scrollEnabled={data.length > 0}
        style={{ flex: 1 }}
        data={data.slice()}
        renderItem={renderItem}
        ListEmptyComponent={() => (
          <Text
            style={styles.emptyText}
            children="Você ainda não possui transferências"
          />
        )}
        keyExtractor={(_, index) => `${index}`}
      />
    </SafeAreaView>
  );
}

export default HomeScreen;
