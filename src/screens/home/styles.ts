import { StyleSheet } from 'react-native';

const defaultFont = {
  fontFamily: 'Poppins-Regular',
  color: '#333333',
  fontSize: 14
};

export default StyleSheet.create({
  card: {
    padding: 16,
    backgroundColor: 'white',
    borderRadius: 6,
    margin: 16,
    marginBottom: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1
  },
  cardSubtitle: {
    ...defaultFont,
    textAlign: 'center',
    opacity: 0.6,
    fontSize: 16
  },
  cardTitle: {
    textAlign: 'center',
    fontFamily: 'Poppins-Bold',
    color: '#2196F3',
    fontSize: 28
  },
  actionContainer: {
    flexDirection: 'row',
    width: '100%',
    marginTop: 16
  },
  actionButton: {
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'rgba(51, 51, 51, 0.2)'
  },
  actionLabel: {
    fontFamily: 'Poppins-Bold',
    color: '#2196F3',
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  cell: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16
  },
  cellStatus: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: 'tomato'
  },
  cellInfo: {
    marginHorizontal: 8,
    flex: 1
  },
  cellDate: {
    ...defaultFont,
    opacity: 0.6
  },
  cellDescription: {
    ...defaultFont
  },
  cellAmount: {
    ...defaultFont,
    fontSize: 18
  },
  emptyText: {
    ...defaultFont,
    fontFamily: 'Poppins-Bold',
    fontSize: 16,
    opacity: 0.6,
    textAlign: 'center',
    margin: 16
  }
});
