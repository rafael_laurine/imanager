import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Alert,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import TransferService from 'services/transfer';
import { converMoney } from 'utils';
import styles from './styles';

type RootStackParamList = {
  Transfer: { type: 'debit' | 'credit' };
};

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'Transfer'>;

function TransferScreen() {
  const navigation = useNavigation();
  const route = useRoute<ProfileScreenRouteProp>();

  const [amount, setAmount] = useState<string>('0');
  const [description, setDescription] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [type, setType] = useState<'debit' | 'credit'>('debit');
  const [title, setTitle] = useState<string>('');

  useEffect(() => {
    const { type } = route.params;
    setType(type);
  }, [route]);

  useEffect(() => {
    const title = type == 'debit' ? 'Depositar' : 'Trasferir';
    setTitle(title);
    navigation.setOptions({ title });
  }, [navigation, type]);

  async function onSubmit() {
    try {
      setLoading(true);

      const value = converMoney(amount);

      if (value <= 0) {
        throw 'É necessário informar um valor!';
      }
      if (description.length <= 0) {
        throw 'É necessário informar um descritivo!';
      }

      await TransferService.sendTransfer({
        amount,
        description,
        type,
        date: new Date()
      });
      let message =
        type == 'debit'
          ? 'Seu depósito foi realizado'
          : 'Sua transferência foi realizada';
      message += 'com sucesso';
      Alert.alert('Parabéns', message);
      navigation.goBack();
    } catch (error) {
      Alert.alert('Erro', error);
    } finally {
      setLoading(false);
    }
  }

  return (
    <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.scrollView}>
      <Text style={styles.label} children="Valor:" />
      <TextInputMask
        style={styles.input}
        type={'money'}
        options={{
          precision: 2,
          separator: ',',
          delimiter: '.',
          unit: 'R$',
          suffixUnit: ''
        }}
        value={amount}
        onChangeText={setAmount}
      />
      <Text style={styles.label} children="Descritivo:" />
      <TextInput
        multiline={true}
        numberOfLines={4}
        style={styles.input}
        value={description}
        onChangeText={setDescription}
      />
      <TouchableOpacity style={styles.actionButton} onPress={onSubmit}>
        {loading ? (
          <ActivityIndicator color="white" />
        ) : (
          <Text style={styles.actionLabel} children={title} />
        )}
      </TouchableOpacity>
    </ScrollView>
  );
}

export default TransferScreen;
