import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  scrollView: {
    flexGrow: 1,
    padding: 16
  },
  label: {
    color: '#333',
    fontFamily: 'Poppins-Bold',
    fontSize: 18,
    marginBottom: 4
  },
  input: {
    color: '#333',
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    width: '100%',
    minHeight: 48,
    marginBottom: 16,
    paddingLeft: 12,
    paddingVertical: 14,
    borderRadius: 2,
    borderColor: 'rgba(51,51,51,0.2)',
    borderWidth: 1
  },
  actionButton: {
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    backgroundColor: '#2196F3'
  },
  actionLabel: {
    fontFamily: 'Poppins-Bold',
    color: 'white',
    textAlign: 'center',
    textTransform: 'uppercase'
  }
});
