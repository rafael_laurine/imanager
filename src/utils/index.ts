export function converMoney(amount: string) {
  let value = amount.replace('R$', '');
  value = value.split('.').join('');
  value = value.replace(',', '.');
  return parseFloat(value || '0');
}
