export interface ITransfer {
  amount: string;
  type: 'credit' | 'debit';
  description: string;
  date: Date;
}
