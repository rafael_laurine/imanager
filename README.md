# IManager

IManager é um projeto visa a criação de uma carteira virtual.

## Instalação

Seguir o passo a passo do [React Native](http://facebook.github.io/react-native/)

## O que foi usado

- [Typescript](https://reactnative.dev/docs/typescript) - typescript é uma extensão do Javascript que adiciona tipagem estática.
- [BabelPluginModuleResolver](https://github.com/tleunen/babel-plugin-module-resolver) - um plugin do Babel para configuração de caminhos absolutos para imports.
- [React Navigation](https://reactnavigation.org/) - Uma lib navegação em Javascript para react native;
- [MomentJS](https://momentjs.com/) - um pacote open source que pode ser utilizado para validar, manipular e fazer o parse de datas no JavaScript;
- [ReactNativeMaskedText](https://github.com/benhurott/react-native-masked-text) - uma lib de máscaras para o text-input;

## Como contribuir

1. Faça um Fork do projeto
2. Crie uma Branch para sua Feature (`git checkout -b feature/algumNome`)
3. Adicione suas mudanças (`git add .`)
4. Comite suas mudanças (`git commit -m 'Informe algo que foi implementado`)
5. Faça o Push da Branch (`git push origin feature/algumNome`)
6. Abra um Pull Request

## Licença

[MIT](https://choosealicense.com/licenses/mit/)
